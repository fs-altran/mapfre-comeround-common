import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComeroundCommonComponent } from './comeround-common.component';

describe('ComeroundCommonComponent', () => {
  let component: ComeroundCommonComponent;
  let fixture: ComponentFixture<ComeroundCommonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComeroundCommonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComeroundCommonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

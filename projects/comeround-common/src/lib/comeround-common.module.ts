import { NgModule } from '@angular/core';
import { ComeroundCommonComponent } from './comeround-common.component';
import { HTTPServices } from './http-services.service';
import { Utils } from './utils.service';
import { Validators } from './validators.service';

@NgModule({
  declarations: [ComeroundCommonComponent],
  imports: [
  ],
  exports: [ComeroundCommonComponent],
  providers: [
    HTTPServices,
    Utils,
    Validators,
  ]
})
export class ComeroundCommonModule { }

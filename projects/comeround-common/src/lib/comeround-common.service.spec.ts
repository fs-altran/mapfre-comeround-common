import { TestBed } from '@angular/core/testing';

import { ComeroundCommonService } from './comeround-common.service';

describe('ComeroundCommonService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ComeroundCommonService = TestBed.get(ComeroundCommonService);
    expect(service).toBeTruthy();
  });
});

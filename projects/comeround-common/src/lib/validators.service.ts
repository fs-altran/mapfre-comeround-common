import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { AbstractControl, ValidatorFn } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class Validators {

  constructor() { }

  isValidBirthdate(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      const date = moment(control.value);
      return date.isValid()
        ? null
        : { invalid: true };
    };
  }

  // code: string, pattern: string
  isValidPostalCode(pattern: any): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
      return pattern.test(control.value)
        ? null
        : { invalid: true };
    };
  }

  isValidIngreso(ingreso: number) {

  }

  isValidEmail(email: string) {

  }

  isValidNif(nif: string) {

  }

  isValidNie(nie: string) {

  }

  isValidPassport(passport: string) {

  }

  getAgeFromBirthdate(date: string) {

  }

}
